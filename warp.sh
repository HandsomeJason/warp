#!/bin/bash

# 设置代码页为中文GBK
export LANG=zh_CN.UTF-8

# 检查是否有管理员权限,没有则获取
if [ $EUID -ne 0 ]; then
  sudo "$0" "$@"
  exit $?
fi

# 定义一些文件夹和文件
warp="/gpt/warp.bat"
ips_v4="/gpt/ips-v4.txt" 
ips_v6="/path/to/ips-v6.txt"
result="/path/to/result.csv"

# 定义一些变量
menu=1
n=0

echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo "::                                                        ::" 
echo "::           欢迎使用Linux平台WARP启动程序             ::"
echo "::           本程序由CF提供免费不限流量服务            ::"
echo "::                         请勿非法滥用                  ::" 
echo "::                                                        ::"
echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
echo 

# 主菜单函数
main_menu() {
  echo "1. WARP-V4优选"
  echo "2. WARP-V6优选" 
  echo "0. 退出"
  echo  
  read -p "请选择菜单(默认 $menu):" choice
  case $choice in
    0) exit 0;;
    1) menu=1; title="WARP-V4优选"; filename=$ips_v4; get_ips;;
    2) menu=2; title="WARP-V6优选"; filename=$ips_v6; get_ips;;
    *) clear; main_menu;;
  esac
}

# 获取IP地址函数  
get_ips() {
  
  > ip.txt
  
  while [ $n -lt 100 ]; do
    random_cidr
    
    if [[ -z $(grep "$ip.$j.$k.$cidr" ip.txt) ]]; then
      echo "$ip.$j.$k.$cidr" >> ip.txt
      let n++ 
    fi

  done
  
  get_best_ip
  
}

# 随机生成CIDR函数
random_cidr() {

  cidr=$((RANDOM%256))
  
}

# 测试延迟选择最佳IP函数
get_best_ip() {

  $warp -i ip.txt -o $result 
  
  while read ip loss delay; do
  
    endpoint=$ip
    loss=$loss
    delay=$delay
    
    break
    
  done < $result
  
  connect
  
}

# 连接最佳IP函数  
connect() {

  warp-cli disconnect
  warp-cli clear-custom-endpoint
  warp-cli set-custom-endpoint $endpoint
  warp-cli connect
  
  > ip.txt
  > $result
  
  echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
  echo "::                                                        ::"
  echo "::           已设置自定义节点为$endpoint"
  echo "::             丢包率 $loss 平均延迟 $delay               ::" 
  echo "::                                                        ::"
  echo "::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
  echo

  read -p "按任意键退出"
  exit 0
  
}

# 主程序
while :; do
  clear
  main_menu
done
